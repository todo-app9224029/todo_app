import { useParams } from "react-router-dom";

function TodoDetails({ todos }) {
  const todoObj = useParams();
  const selectedTodo = todos.find((todo) => todo.id == todoObj.todoId);
  return (
    <div className="container text-start">
      {selectedTodo ? (
        <>
          <h3>Todo Details</h3>
          <div>
            <label>Title : </label> <span>{selectedTodo.title}</span>
          </div>

          <div>
            <label>Description : </label>{" "}
            <span>{selectedTodo.description}</span>
          </div>

          <div>
            <label>Date : </label> <span>{selectedTodo.date}</span>
          </div>

          <div>
            <label>State : </label> <span>{selectedTodo.state}</span>
          </div>

          <div>
            <label>Priority : </label> <span>{selectedTodo.Priority}</span>
          </div>

          <div>
            <label>Group : </label> <span>{selectedTodo.group}</span>
          </div>
        </>
      ) : (
        <div>no Result for this ID</div>
      )}
    </div>
  );
}

export default TodoDetails;
