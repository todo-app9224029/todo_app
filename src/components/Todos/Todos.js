import { Link } from "react-router-dom";
import { useState } from "react";

function Todos({
  todos,
  onDeleteTodos,
  onTitleFilter,
  onDateFilter,
  onTodaysTodos,
  onweekTodos,
  titleValueFilter,
  dateValueFilter,
  onChangeStatus,
}) {
  const [selectedTodos, setSelectedTodos] = useState([]);

  const onCheckedChange = (todo) => {
    if (!selectedTodos.includes(todo.id)) {
      setSelectedTodos((selectedTodos) => {
        return [...selectedTodos, todo.id];
      });
    }
    if (selectedTodos.includes(todo.id)) {
      setSelectedTodos((selectedTodos) =>
        selectedTodos.filter((selectedTodo) => selectedTodo !== todo.id)
      );
    }
  };

  const deleteTodos = (selectedTodos) => {
    onDeleteTodos(selectedTodos);
  };

  const changeStatus = (selectedTodos) => {
    onChangeStatus(selectedTodos);
  };

  const titleFilterHandler = (e) => {
    onTitleFilter(e.target.value);
  };

  const dateFilterHandler = (e) => {
    onDateFilter(e.target.value);
  };

  const todayHandler = () => {
    onTodaysTodos();
  };

  const weekTodayHandler = () => {
    onweekTodos();
  };

  return (
    <div className="container ">
      <div className=" my-5">
        <input
          type="text"
          required
          className="form-control mb-3"
          id="titleFilteration "
          placeholder="Filter by title"
          value={titleValueFilter}
          onChange={(e) => {
            titleFilterHandler(e);
          }}
        />

        <input
          type="date"
          required
          className="form-control mb-3"
          id="date"
          placeholder="Filter by date"
          value={dateValueFilter}
          onChange={(e) => dateFilterHandler(e)}
        />

        <button
          type="button"
          className="btn btn-success mx-2"
          onClick={todayHandler}
        >
          Today
        </button>

        <button
          type="button"
          className="btn btn-success mx-2"
          onClick={weekTodayHandler}
        >
          Week
        </button>

        {todos.map((todo) => {
          return (
            <div
              key={todo.id}
              className="d-flex justify-content-between border rounded p-3 my-3"
            >
              <div className="d-flex">
                <span className="form-check">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    value=""
                    onChange={() => onCheckedChange(todo)}
                  />
                </span>
                <Link to={`/tododetails/${todo.id}`} state={{ todo: todo }}>
                  {todo.title}
                </Link>

                <span className="mx-2">{todo.date}</span>
                {" - "}
                <span className="mx-2">{todo.state}</span>
              </div>
              <div>
                <button
                  type="button"
                  className="btn btn-success mx-2"
                  disabled={selectedTodos.includes(todo.id) ? false : true}
                  onClick={() => changeStatus(todo)}
                >
                  Done
                </button>
                <button
                  type="button"
                  className="btn btn-danger"
                  disabled={selectedTodos.includes(todo.id) ? false : true}
                  onClick={() => deleteTodos(todo)}
                >
                  Delete
                </button>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Todos;
