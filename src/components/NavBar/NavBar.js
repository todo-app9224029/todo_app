import { Link } from "react-router-dom";
function NavBar() {
  return (
    <div className="bg-light text-start p-4" style={{ height: "100vh" }}>
      <Link className="navbar-brand" to="/">
        ToDos
      </Link>

      <Link className="nav-link active" aria-current="page" to="/addtodo">
        Add Todo
      </Link>
    </div>
  );
}

export default NavBar;
