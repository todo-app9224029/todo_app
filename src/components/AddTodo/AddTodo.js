import { useState } from "react";
import { Link } from "react-router-dom";
import NavBar from "../NavBar/NavBar";

const initialState = {
  id: "",
  title: "",
  description: "",
  date: "",
  state: "Not started",
  Priority: "",
  group: "WorkGroup",
};

function AddTodo({ onAddTodo }) {
  const [todoObj, setTodoObj] = useState(initialState);
  // const navigate = useNavigate();
  const changeHandler = (input, value) => {
    setTodoObj((todoObj) => {
      return {
        ...todoObj,
        id: Math.floor(Date.now()),
        [input]: value,
      };
    });
  };

  const onPriorityChange = (e) => {
    setTodoObj((todoObj) => {
      return {
        ...todoObj,
        Priority: e.target.value,
      };
    });
  };

  const submitHandler = (e) => {
    e.preventDefault();
    onAddTodo(todoObj);
    setTodoObj(initialState);
  };

  const resetHandler = () => {
    setTodoObj(initialState);
  };

  return (
    <div className="container ">
      {/* <NavBar /> */}
      <div>
        <form onSubmit={submitHandler} className="text-start">
          <div className="mb-3">
            <label htmlFor="title" className="form-label">
              Title
            </label>
            <input
              type="text"
              required
              className="form-control"
              id="title"
              placeholder="title"
              value={todoObj.title}
              onChange={(e) => changeHandler("title", e.target.value)}
            />
          </div>

          <div className="mb-3">
            <label htmlFor="description" className="form-label">
              Description
            </label>
            <textarea
              type="text"
              required
              rows={4}
              className="form-control"
              id="description"
              placeholder="description"
              value={todoObj.description}
              onChange={(e) => changeHandler("description", e.target.value)}
            />
          </div>

          <div className="mb-3">
            <label htmlFor="date" className="form-label">
              Delivery Date
            </label>
            <input
              type="date"
              required
              className="form-control"
              id="date"
              placeholder="22-2-2022"
              value={todoObj.date}
              onChange={(e) => changeHandler("date", e.target.value)}
            />
          </div>

          <div className="mb-3">
            <label className="form-label">Priority</label>
            <div className="form-check">
              <input
                className="form-check-input"
                required
                type="radio"
                name="Priority"
                id="Low"
                value="Low"
                checked={todoObj.Priority === "Low"}
                onChange={onPriorityChange}
              />
              <label className="form-check-label" htmlFor="Low">
                Low
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                required
                type="radio"
                name="Priority"
                id="Medium"
                value="Medium"
                checked={todoObj.Priority === "Medium"}
                onChange={onPriorityChange}
              />
              <label className="form-check-label" htmlFor="Medium">
                Medium
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                required
                type="radio"
                name="Priority"
                id="High"
                value="High"
                checked={todoObj.Priority === "High"}
                onChange={onPriorityChange}
              />
              <label className="form-check-label" htmlFor="High">
                High
              </label>
            </div>
          </div>
          <div className="mb-3">
            <select
              className="form-select"
              aria-label="Default select example"
              required
              name="group"
              id="group"
              value={todoObj.group}
              onChange={(e) => changeHandler("group", e.target.value)}
            >
              <option value="WorkGroup">Work Group</option>
              <option value="HomeGroup">Home Group</option>
              <option value="FunGroup">Fun Group</option>
            </select>
          </div>

          <div>
            <button type="submit" className="btn btn-secondary mx-1">
              Add
            </button>
            <button
              type="button"
              className="btn btn-dark mx-1"
              onClick={resetHandler}
            >
              Clear
            </button>
            <Link to="/">
              <button type="button" className="btn btn-info mx-1">
                Back to list
              </button>
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AddTodo;
