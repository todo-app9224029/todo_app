import { Switch, Route, BrowserRouter } from "react-router-dom";
import moment from "moment";
import "./App.css";
import Todos from "./components/Todos/Todos";
import AddTodo from "./components/AddTodo/AddTodo";
import TodoDetails from "./components/TodoDetails/TodoDetails";
import { useState } from "react";
import NavBar from "./components/NavBar/NavBar";

function App() {
  const [todos, setTodos] = useState([
    {
      id: 1,
      title: "todo Title 1",
      description: "todo description 1",
      date: "2023-07-16",
      state: "not started",
      Priority: "todo Priority 1",
      group: "WorkGroup",
    },
    {
      id: 2,
      title: "todo Title 2",
      description: "todo description 2",
      date: "2023-07-18",
      state: "not started",
      Priority: "todo Priority 2",
      group: "WorkGroup",
    },
    {
      id: 3,
      title: "todo Title 3",
      description: "todo description 3",
      date: "2023-07-20",
      state: "not started",
      Priority: "todo Priority 3",
      group: "WorkGroup",
    },
    {
      id: 4,
      title: "todo Title 4",
      description: "todo description 4",
      date: "2023-07-29",
      state: "not started",
      Priority: "todo Priority 4",
      group: "WorkGroup",
    },
    {
      id: 5,
      title: "todo Title 5",
      description: "todo description 5",
      date: "2023-07-15",
      state: "not started",
      Priority: "todo Priority 5",
      group: "WorkGroup",
    },
    {
      id: 6,
      title: "todo Title 6",
      description: "todo description 6",
      date: "2023-07-12",
      state: "not started",
      Priority: "todo Priority 6",
      group: "WorkGroup",
    },
  ]);
  const [filterResult, setFilterResult] = useState([...todos]);

  const [titleValueFilter, setTitleValueFilter] = useState("");
  const [dateValueFilter, setDateValueFilter] = useState("");

  const addTodo = (todo) => {
    setFilterResult((todos) => {
      return [...todos, todo];
    });
  };

  const deleteTodos = (deletedTodos) => {
    setFilterResult((todos) =>
      todos.filter((todo) => todo.id !== deletedTodos.id)
    );
  };

  const changeStatus = (changedTodos) => {
    const newState = todos.map((todo) => {
      if (todo.id === changedTodos.id) {
        return { ...todo, state: "Done" };
      }
      return todo;
    });
    setFilterResult(newState);
    setTodos(newState);
  };

  const filterByTitle = (titleVal) => {
    const filteredTitles = todos.filter((todo) =>
      todo.title.includes(titleVal)
    );
    setFilterResult(filteredTitles);
    setTitleValueFilter(titleVal);
  };

  const filterByDate = (dateVal) => {
    const filteredDates = todos.filter((todo) => todo.date.includes(dateVal));
    setFilterResult(filteredDates);
    setDateValueFilter(dateVal);
  };

  const todaysTodos = () => {
    const todayTodos = todos.filter((todo) =>
      moment().isSame(todo.date, "day")
    );
    setFilterResult(todayTodos);
  };

  const weekTodos = () => {
    const weekTodos = todos.filter((todo) =>
      moment().isSame(todo.date, "week")
    );
    setFilterResult(weekTodos);
  };

  return (
    <div className="App d-flex">
      <BrowserRouter>
        <NavBar />
        <Switch>
          <Route exact path="/">
            <Todos
              todos={filterResult}
              onDeleteTodos={deleteTodos}
              onTitleFilter={filterByTitle}
              onDateFilter={filterByDate}
              onTodaysTodos={todaysTodos}
              onweekTodos={weekTodos}
              titleValueFilter={titleValueFilter}
              dateValueFilter={dateValueFilter}
              onChangeStatus={changeStatus}
            />
          </Route>
          <Route path="/addtodo">
            <AddTodo onAddTodo={addTodo} />
          </Route>

          <Route path="/tododetails/:todoId">
            <TodoDetails todos={filterResult} />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
